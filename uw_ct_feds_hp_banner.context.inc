<?php

/**
 * @file
 * uw_ct_feds_hp_banner.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_hp_banner_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_banner';
  $context->description = 'Homepage secondary banner for under hero section';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_homepage_banner-block' => array(
          'module' => 'views',
          'delta' => 'feds_homepage_banner-block',
          'region' => 'feds_hero',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Homepage secondary banner for under hero section');
  $export['feds_homepage_banner'] = $context;

  return $export;
}
