<?php

/**
 * @file
 * uw_ct_feds_hp_banner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_hp_banner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_feds_hp_banner_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_hp_banner_node_info() {
  $items = array(
    'feds_homepage_banner' => array(
      'name' => t('Feds Homepage Banner'),
      'base' => 'node_content',
      'description' => t('If required, this homepage banner will provide secondary feature content to display under the split scree hero section. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_feds_hp_banner_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: feds_homepage_banner
  $schemaorg['node']['feds_homepage_banner'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_hp_banner_show_cta' => array(
      'predicates' => array(),
    ),
    'field_cta_text' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_cta_link' => array(
      'predicates' => array(),
    ),
    'field_hp_ban_nercta_text' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_cta_text' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_cta_colour' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_bg_colour' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_bg_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_content_text_colour' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_layout_option' => array(
      'predicates' => array(),
    ),
    'field_background_image_focal_poi' => array(
      'predicates' => array(),
    ),
    'field_hp_banner_bg_image_focal' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
