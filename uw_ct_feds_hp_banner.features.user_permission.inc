<?php

/**
 * @file
 * uw_ct_feds_hp_banner.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_banner_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_homepage_banner content'.
  $permissions['create feds_homepage_banner content'] = array(
    'name' => 'create feds_homepage_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_homepage_banner content'.
  $permissions['delete any feds_homepage_banner content'] = array(
    'name' => 'delete any feds_homepage_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_homepage_banner content'.
  $permissions['delete own feds_homepage_banner content'] = array(
    'name' => 'delete own feds_homepage_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_homepage_banner content'.
  $permissions['edit any feds_homepage_banner content'] = array(
    'name' => 'edit any feds_homepage_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_homepage_banner content'.
  $permissions['edit own feds_homepage_banner content'] = array(
    'name' => 'edit own feds_homepage_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_homepage_banner revision log entry'.
  $permissions['enter feds_homepage_banner revision log entry'] = array(
    'name' => 'enter feds_homepage_banner revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner authored by option'.
  $permissions['override feds_homepage_banner authored by option'] = array(
    'name' => 'override feds_homepage_banner authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner authored on option'.
  $permissions['override feds_homepage_banner authored on option'] = array(
    'name' => 'override feds_homepage_banner authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner comment setting option'.
  $permissions['override feds_homepage_banner comment setting option'] = array(
    'name' => 'override feds_homepage_banner comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner promote to front page option'.
  $permissions['override feds_homepage_banner promote to front page option'] = array(
    'name' => 'override feds_homepage_banner promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner published option'.
  $permissions['override feds_homepage_banner published option'] = array(
    'name' => 'override feds_homepage_banner published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner revision option'.
  $permissions['override feds_homepage_banner revision option'] = array(
    'name' => 'override feds_homepage_banner revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_banner sticky option'.
  $permissions['override feds_homepage_banner sticky option'] = array(
    'name' => 'override feds_homepage_banner sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_homepage_banner content'.
  $permissions['search feds_homepage_banner content'] = array(
    'name' => 'search feds_homepage_banner content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
