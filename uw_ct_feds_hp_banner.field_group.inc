<?php

/**
 * @file
 * uw_ct_feds_hp_banner.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_hp_banner_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hp_banner_background|node|feds_homepage_banner|form';
  $field_group->group_name = 'group_hp_banner_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_banner';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Background',
    'weight' => '8',
    'children' => array(
      0 => 'field_hp_banner_bg_colour',
      1 => 'field_hp_banner_bg_image',
      2 => 'field_hp_banner_bg_image_focal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hp-banner-background field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_hp_banner_background|node|feds_homepage_banner|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hp_banner_content|node|feds_homepage_banner|form';
  $field_group->group_name = 'group_hp_banner_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_banner';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '6',
    'children' => array(
      0 => 'body',
      1 => 'field_content_text_colour',
      2 => 'field_hp_banner_layout_option',
      3 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hp-banner-content field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_hp_banner_content|node|feds_homepage_banner|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hp_banner_cta|node|feds_homepage_banner|form';
  $field_group->group_name = 'group_hp_banner_cta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_banner';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Call to Action (CTA) Banner',
    'weight' => '7',
    'children' => array(
      0 => 'field_hp_banner_show_cta',
      1 => 'field_hp_banner_cta_link',
      2 => 'field_hp_banner_cta_text',
      3 => 'field_hp_banner_cta_colour',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hp-banner-cta field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_hp_banner_cta|node|feds_homepage_banner|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Background');
  t('Call to Action (CTA) Banner');
  t('Content');

  return $field_groups;
}
