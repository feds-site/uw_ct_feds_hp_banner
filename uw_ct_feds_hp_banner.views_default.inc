<?php

/**
 * @file
 * uw_ct_feds_hp_banner.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_hp_banner_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_homepage_banner';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Homepage Banner';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds Homepage Banner';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_type'] = 'div';
  $handler->display->display_options['fields']['body']['element_class'] = 'banner-body';
  /* Field: Content: Content Text Colour */
  $handler->display->display_options['fields']['field_content_text_colour']['id'] = 'field_content_text_colour';
  $handler->display->display_options['fields']['field_content_text_colour']['table'] = 'field_data_field_content_text_colour';
  $handler->display->display_options['fields']['field_content_text_colour']['field'] = 'field_content_text_colour';
  $handler->display->display_options['fields']['field_content_text_colour']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_content_text_colour']['type'] = 'list_key';
  /* Field: Content: Layout Options */
  $handler->display->display_options['fields']['field_hp_banner_layout_option']['id'] = 'field_hp_banner_layout_option';
  $handler->display->display_options['fields']['field_hp_banner_layout_option']['table'] = 'field_data_field_hp_banner_layout_option';
  $handler->display->display_options['fields']['field_hp_banner_layout_option']['field'] = 'field_hp_banner_layout_option';
  $handler->display->display_options['fields']['field_hp_banner_layout_option']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_hp_banner_layout_option']['type'] = 'list_key';
  /* Field: Content: Show CTA */
  $handler->display->display_options['fields']['field_hp_banner_show_cta']['id'] = 'field_hp_banner_show_cta';
  $handler->display->display_options['fields']['field_hp_banner_show_cta']['table'] = 'field_data_field_hp_banner_show_cta';
  $handler->display->display_options['fields']['field_hp_banner_show_cta']['field'] = 'field_hp_banner_show_cta';
  $handler->display->display_options['fields']['field_hp_banner_show_cta']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_hp_banner_show_cta']['type'] = 'list_key';
  /* Field: Content: CTA Text */
  $handler->display->display_options['fields']['field_hp_banner_cta_text']['id'] = 'field_hp_banner_cta_text';
  $handler->display->display_options['fields']['field_hp_banner_cta_text']['table'] = 'field_data_field_hp_banner_cta_text';
  $handler->display->display_options['fields']['field_hp_banner_cta_text']['field'] = 'field_hp_banner_cta_text';
  $handler->display->display_options['fields']['field_hp_banner_cta_text']['exclude'] = TRUE;
  /* Field: Content: CTA Link */
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['id'] = 'field_hp_banner_cta_link';
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['table'] = 'field_data_field_hp_banner_cta_link';
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['field'] = 'field_hp_banner_cta_link';
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_hp_banner_cta_link']['settings'] = array(
    'custom_title' => '',
  );
  /* Field: Content: CTA Colour Selection */
  $handler->display->display_options['fields']['field_hp_banner_cta_colour']['id'] = 'field_hp_banner_cta_colour';
  $handler->display->display_options['fields']['field_hp_banner_cta_colour']['table'] = 'field_data_field_hp_banner_cta_colour';
  $handler->display->display_options['fields']['field_hp_banner_cta_colour']['field'] = 'field_hp_banner_cta_colour';
  $handler->display->display_options['fields']['field_hp_banner_cta_colour']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_hp_banner_cta_colour']['type'] = 'list_key';
  /* Field: Content: Background Colour */
  $handler->display->display_options['fields']['field_hp_banner_bg_colour']['id'] = 'field_hp_banner_bg_colour';
  $handler->display->display_options['fields']['field_hp_banner_bg_colour']['table'] = 'field_data_field_hp_banner_bg_colour';
  $handler->display->display_options['fields']['field_hp_banner_bg_colour']['field'] = 'field_hp_banner_bg_colour';
  $handler->display->display_options['fields']['field_hp_banner_bg_colour']['exclude'] = TRUE;
  /* Field: Content: Background Image */
  $handler->display->display_options['fields']['field_hp_banner_bg_image']['id'] = 'field_hp_banner_bg_image';
  $handler->display->display_options['fields']['field_hp_banner_bg_image']['table'] = 'field_data_field_hp_banner_bg_image';
  $handler->display->display_options['fields']['field_hp_banner_bg_image']['field'] = 'field_hp_banner_bg_image';
  $handler->display->display_options['fields']['field_hp_banner_bg_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_hp_banner_bg_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_hp_banner_bg_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Background Image Focal Point */
  $handler->display->display_options['fields']['field_hp_banner_bg_image_focal']['id'] = 'field_hp_banner_bg_image_focal';
  $handler->display->display_options['fields']['field_hp_banner_bg_image_focal']['table'] = 'field_data_field_hp_banner_bg_image_focal';
  $handler->display->display_options['fields']['field_hp_banner_bg_image_focal']['field'] = 'field_hp_banner_bg_image_focal';
  $handler->display->display_options['fields']['field_hp_banner_bg_image_focal']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_hp_banner_bg_image_focal']['type'] = 'list_key';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body_1']['id'] = 'body_1';
  $handler->display->display_options['fields']['body_1']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body_1']['field'] = 'body';
  $handler->display->display_options['fields']['body_1']['label'] = '';
  $handler->display->display_options['fields']['body_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body_1']['alter']['text'] = '<div class="hp-banner-container">
  <div class="hp-banner-bg-color bgalign-[field_hp_banner_bg_image_focal]">[field_hp_banner_bg_image][field_hp_banner_bg_colour]</div>
  <div class="hp-banner-content txt-color-[field_content_text_colour] banner-layout-[field_hp_banner_layout_option]">
    <h2>[title]</h2>
    <div class="banner-body">[body]</div>
    <div class="banner-cta cta-visibility-[field_hp_banner_show_cta] cta-theme-[field_hp_banner_cta_colour]">
      <a href="[field_hp_banner_cta_link]">[field_hp_banner_cta_text]</a>
    </div>
  </div>
</div>';
  $handler->display->display_options['fields']['body_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body_1']['element_default_classes'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_homepage_banner' => 'feds_homepage_banner',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['feds_homepage_banner'] = array(
    t('Master'),
    t('Feds Homepage Banner'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Body'),
    t('Content Text Colour'),
    t('Layout Options'),
    t('Show CTA'),
    t('CTA Text'),
    t('CTA Link'),
    t('CTA Colour Selection'),
    t('Background Colour'),
    t('Background Image'),
    t('Background Image Focal Point'),
    t('<div class="hp-banner-container">
  <div class="hp-banner-bg-color bgalign-[field_hp_banner_bg_image_focal]">[field_hp_banner_bg_image][field_hp_banner_bg_colour]</div>
  <div class="hp-banner-content txt-color-[field_content_text_colour] banner-layout-[field_hp_banner_layout_option]">
    <h2>[title]</h2>
    <div class="banner-body">[body]</div>
    <div class="banner-cta cta-visibility-[field_hp_banner_show_cta] cta-theme-[field_hp_banner_cta_colour]">
      <a href="[field_hp_banner_cta_link]">[field_hp_banner_cta_text]</a>
    </div>
  </div>
</div>'),
    t('Block'),
  );
  $export['feds_homepage_banner'] = $view;

  return $export;
}
